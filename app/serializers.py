from rest_framework import serializers

from .models import Enlace, Categoria
from django.contrib.auth.models import User

class EnlaceSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Enlace
		fields = ('url', 'id', 'titulo', 'categoria', 'enlace', 'votos', 'usuario', 'timestamp')

class UserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ('url', 'username', 'email')

class CategoriaSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Categoria
		fields = ('url', 'titulo')